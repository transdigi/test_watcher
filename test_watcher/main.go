package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"test_watcher/util"

	"cloud.google.com/go/pubsub"
	"github.com/fsnotify/fsnotify"
)

const (
	ProjectId   = "dataintegration-072018"
	watchFolder = "/home/trans-backend/watchthisbucket/"
)

// main
func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// os.Exit(2)
	// for pubsub
	ctx := context.Background()
	proj := ProjectId
	if proj == "" {
		fmt.Fprintf(os.Stderr, "GOOGLE_CLOUD_PROJECT environment variable must be set.\n")
		os.Exit(1)
	}
	client, err := pubsub.NewClient(ctx, proj)

	if err != nil {
		log.Fatalf("Could not create pubsub Client: %v", err)
	}
	const topic = "kevin-topic"
	util.ReadDirectoryFirst(watchFolder, topic, client)

	// if err := publish(client, topic, "hello genks!"); err != nil {
	// 	log.Fatalf("Failed to publish: %v", err)
	// }

	// watch
	// creates a new file watcher
	log.Print("watch folder", watchFolder)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Println("ERROR", err)
	}
	defer watcher.Close()

	//
	done := make(chan bool)

	//
	go func() {
		for {
			select {
			// watch for events
			case event := <-watcher.Events:
				content := util.ProcessingFile(event.Name)
				if content != nil {

					if err := util.Publish(client, topic, string(content)); err != nil {
						log.Fatalf("Failed to publish: %v", err)
					}
				}
				// log.Printf("EVENT! %#v\n", event)

				// watch for errors
			case err := <-watcher.Errors:
				log.Println("ERROR", err)
			}
		}
	}()

	// out of the box fsnotify can watch a single file, or a single directory
	if err := watcher.Add(watchFolder); err != nil {
		log.Println("ERROR", err)
	}

	<-done
}
