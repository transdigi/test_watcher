// Sample storage-quickstart creates a Google Cloud Storage bucket.
package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"runtime"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

func main() {
	ctx := context.Background()

	// Sets your Google Cloud Platform project ID.
	// projectID := "dataintegration-072018"

	// Creates a client.
	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	// Sets the name for the new bucket.
	bucketName := "td-datahub"
	subfolder := "transmart/transaction/2018"
	// Creates a Bucket instance.
	// bucket := client.Buckets(ctx, bucketName)
	// query := &storage.Query{}
	// projectBucket := fmt.Sprintf("%v/%v", bucketName, subfolder)
	// fmt.Println(projectBucket)
	it := client.Bucket(bucketName).Objects(ctx, &storage.Query{
		Prefix: subfolder,
	})
	PrintMemUsage()

	i := 0
	for i <= 100 {

		attrs, err := it.Next()
		read(client, bucketName, attrs.Name)

		if err == iterator.Done {
			break
		}
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("name", attrs.Name)
		i++
		PrintMemUsage()
	}
	PrintMemUsage()
	// Creates the new bucket.
	// if err := bucket.Create(ctx, projectID, nil); err != nil {
	// 	log.Fatalf("Failed to create bucket: %v", err)
	// }

	// fmt.Printf("Bucket %v created.\n", bucketName)
}

func read(client *storage.Client, bucket, object string) ([]byte, error) {
	ctx := context.Background()
	// [START download_file]
	rc, err := client.Bucket(bucket).Object(object).NewReader(ctx)
	if err != nil {
		return nil, err
	}
	defer rc.Close()

	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return nil, err
	}
	// fmt.Println(string(data))
	return data, nil
	// [END download_file]
}

func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
	fmt.Printf("\tNumGC = %v\n", m.NumGC)
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
